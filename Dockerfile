FROM node:20-alpine
ENV NODE_ENV=production
WORKDIR /app
COPY ["package.json", "package-lock.json", "./"]
RUN npm ci --only=production
USER node
COPY . .
EXPOSE 5000
CMD ["node", "server.js"]
